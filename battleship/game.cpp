#include "game.h"

Game::Game() {
	b.set(15, 10, '.', 'O', 'X', '/', 2);
	total_points = 0;
	int* ships = b.get_ship_sizes();
	cout << "br brodova:" << b.get_ship_number() << endl;
	for (int i = 0; i < b.get_ship_number(); i++) {
		total_points += ships[i];

	}
	player_points = 0;
	computer_points = 0;
}

void Game::start() {
	player.set_ships(b);
	computer.set_ships(b);

	while (player_points < total_points && computer_points < total_points) {
		cout << "------ " << computer.get_username() << " : " << computer_points << " ------" << endl;
		computer.print_board(b);
		cout << endl << endl << "------ " << player.get_username() << " : " << player_points << " ------" << endl;
		player.print_board(b);
		bool win, check = 1;

		cout << "Vas potez! Pogodite koordinatu: ";
		string s;
		cin >> s;
		if (s.size() != 2) {
			cout << "Error! Neispravan unos." << endl;
			check = 0;
			sleep_for(nanoseconds(10));
			sleep_until(system_clock::now() + seconds(1));
			clear_screen();
			continue;
		}
		int* coordinates = coordinate_convert(s);
		int coord_x = coordinates[0];
		int coord_y = coordinates[1];

		check = player.check_move(b, computer, coord_x, coord_y);

		if (!check) {
			cout << "Error! Los unos." << endl;
			sleep_for(nanoseconds(10));
			sleep_until(system_clock::now() + seconds(1));
			clear_screen();
		}
		else {
			win = computer.is_hit(coord_x, coord_y);
			if (win) {
				player_points++;
			}

			coordinates = computer.smart_move(b, player);
			coord_x = coordinates[0];
			coord_y = coordinates[1];

			win = player.is_hit(coord_x, coord_y);
			if (win) {
				computer_points++;
			}
			sleep_for(nanoseconds(10));
			sleep_until(system_clock::now() + seconds(1));
			clear_screen();
		}
	}
	if (computer_points < player_points)
		cout << endl << endl << "******************" << "Pobjeda!" << "******************" << endl;
	else
		cout << endl << endl << "******************" << "Poraz!" << "******************" << endl;
}