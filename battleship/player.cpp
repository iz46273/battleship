#include "player.h"

void Player::print_board(Board b) {
	cout << "  ";
	for (int i = 0; i < b.get_width(); i++) {
		cout << number_to_letter(i) << " ";
	}
	cout << endl;

	for (int i = 0; i < b.get_height(); i++) {
		cout << number_to_letter(i) << " ";
		for (int j = 0; j < b.get_width(); j++) {
			if (ships[j][i] == 0)
				cout << b.get_empty_field() << " ";
			if (ships[j][i] == 1)
				cout << b.get_taken_field() << " ";
			if (ships[j][i] == 2)
				cout << b.get_hit_field() << " ";
			if (ships[j][i] == 3)
				cout << b.get_missed_field() << " ";
		}
		cout << endl;
	}
}

bool Player::overlap_check(Board b, int** new_ship) {
	for (int i = 0; i < b.get_width(); i++) {
		for (int j = 0; j < b.get_height(); j++) {
			if (new_ship[i][j] == 1 && ships[i][j] == 1)
				return 0;
		}
	}
	return 1;
}

bool Player::check_if_overboard(Board b, int x, int y, bool position, int size) {
	if (position) { //okomito
		if (x >= 0 && x <= b.get_width()) {
			if (y >= 0 && y + size <= b.get_height())
				return 1;
		}
	}

	else { //vodoravno
		if (x >= 0 && x + size <= b.get_width()) {
			if (y >= 0 && y <= b.get_height())
				return 1;
		}
	}
	return 0;
}

void Player::set_new_ship(Board b, int** new_ship) {
	for (int i = 0; i < b.get_width(); i++) {
		for (int j = 0; j < b.get_height(); j++) {
			if (new_ship[i][j] == 1)
				ships[i][j] = 1;
		}
	}
}

void Player::set_ships(Board b) {
	int* ship_sizes = b.get_ship_sizes();
	ships = new int* [b.get_width()];
	for (int i = 0; i < b.get_width(); i++)
		ships[i] = new int[b.get_height()];

	for (int i = 0; i < b.get_width(); i++) {
		for (int j = 0; j < b.get_height(); j++) {
			ships[i][j] = 0;
		}
	}

	for (int i = 0; i < b.get_ship_number(); i++) {
		int size = ship_sizes[i];
		bool check = add_ship(size, b);
		if (!check)
			i--;
	}
}

bool Player::is_hit(int x, int y) {
	if (ships[x][y] == 1) {
		ships[x][y] = 2;
		return 1;
	}
	else {
		ships[x][y] = 3;
		return 0;
	}
}

bool Player::hit_same_field_check(int x, int y) {
	if (ships[x][y] == 2 || ships[x][y] == 3)
		return 0;
	return 1;
}

bool Player::check_move(Board b, Player& enemy, int x, int y) {
	bool check = 1;
	if (x < 0 || x > b.get_width() || y < 0 || y > b.get_height()) {
		check = 0;
	}
	if (check) {
		check = enemy.hit_same_field_check(x, y);
	}
	return check;
}

HumanPlayer::HumanPlayer() {
	string str;
	cout << "Username: ";
	cin >> str;
	username = str;
}

bool HumanPlayer::input_new_ship(string& coordinate, bool& position, int size) {
	cout << endl << "Brod: ";
	for (int j = 0; j < size; j++)
		cout << "O";
	cout << endl;
	cout << "Unesite koordinatu pocetka broda: ";
	coordinate;
	cin >> coordinate;
	if (coordinate.size() != 2) {
		cout << "Neispravan unos." << endl;
		return 0;
	}

	cout << "0 - vodoravno" << endl << "1 - okomito" << endl << "Vodoravno ili okomito: ";
	position;
	cin >> position;
	return 1;
}

bool HumanPlayer:: add_ship(int size, Board b) {
	string coordinate;
	bool position, check;

	print_board(b);

	check = input_new_ship(coordinate, position, size);
	if (!check) {
		cout << "Error! Neispravan unos." << endl;
		sleep_for(nanoseconds(10));
		sleep_until(system_clock::now() + seconds(1));
		clear_screen();
		return 0;
	}

	int* coordinates = coordinate_convert(coordinate);
	int coordinate_x = coordinates[0];
	int coordinate_y = coordinates[1];

	check = check_if_overboard(b, coordinate_x, coordinate_y, position, size);
	if (!check) {
		cout << "Error! Brod prelazi plocu." << endl;
		sleep_for(nanoseconds(10));
		sleep_until(system_clock::now() + seconds(1));
		clear_screen();
		return 0;
	}

	int** new_ship = new int* [b.get_width()];
	for (int i = 0; i < b.get_width(); i++)
		new_ship[i] = new int[b.get_height()];

	if (position) { //okomito
		for (int y = coordinate_y; y < coordinate_y + size; y++)
			new_ship[coordinate_x][y] = 1;
	}

	else { //vodoravno
		for (int x = coordinate_x; x < coordinate_x + size; x++)
			new_ship[x][coordinate_y] = 1;
	}

	check = overlap_check(b, new_ship);
	if (!check) {
		cout << "Error! Brodovi se preklapaju." << endl;
		sleep_for(nanoseconds(10));
		sleep_until(system_clock::now() + seconds(1));
		clear_screen();
		return 0;
	}

	set_new_ship(b, new_ship);
	delete[] new_ship;
	cout << "Dodavanje..." << endl;
	sleep_for(nanoseconds(10));
	sleep_until(system_clock::now() + seconds(1));
	clear_screen();
	return 1;
}

bool ComputerPlayer::add_ship(int size, Board b) {
	string coordinate;
	bool position, check;

	int coordinate_x = rand() % b.get_width();
	int coordinate_y = rand() % b.get_height();
	position = rand() > (RAND_MAX / 2);

	check = check_if_overboard(b, coordinate_x, coordinate_y, position, size);
	if (!check) {
		return 0;
	}

	int** new_ship = new int* [b.get_width()];
	for (int i = 0; i < b.get_width(); i++)
		new_ship[i] = new int[b.get_height()];

	if (position) { //okomito
		int x = coordinate_x;
		for (int y = coordinate_y; y < coordinate_y + size; y++)
			new_ship[x][y] = 1;
	}

	else { //vodoravno
		int y = coordinate_y;
		for (int x = coordinate_x; x < coordinate_x + size; x++)
			new_ship[x][y] = 1;
	}

	check = overlap_check(b, new_ship);
	if (!check) {
		return 0;
	}

	set_new_ship(b, new_ship);
	delete[] new_ship;
	return 1;
}

int* ComputerPlayer::smart_move(Board b, Player& enemy) {
	int check;
	int** enemy_ships = enemy.get_ships();
	int** potential_moves;
	potential_moves = new int* [4];
	for (int i = 0; i < 4; i++) {
		potential_moves[i] = new int[2];
	}

	for (int i = 0; i < b.get_width(); i++) {
		for (int j = 0; j < b.get_height(); j++) {
			if (enemy_ships[i][j] == 2) {
				potential_moves[0][0] = i + 1;
				potential_moves[0][1] = j;

				potential_moves[1][0] = i - 1;
				potential_moves[1][1] = j;

				potential_moves[2][0] = i;
				potential_moves[2][1] = j + 1;

				potential_moves[3][0] = i;
				potential_moves[3][1] = j - 1;

				for (int k = 0; k < 4; k++) {
					check = check_move(b, enemy, potential_moves[k][0], potential_moves[k][1]);
					if (check) {
						int ret[] = { potential_moves[k][0], potential_moves[k][1] };
						return ret;
					}
				}
			}
		}
	}
	int coord_x = rand() % b.get_width();
	int coord_y = rand() % b.get_height();
	int ret[] = { coord_x, coord_y };
	return ret;
}