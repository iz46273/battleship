#include <iostream>
#include <string>
#include <cctype>
#include <random>
#include <chrono>
#include <thread>
#pragma warning (disable: 4669)
using namespace std;
using namespace std::this_thread;
using namespace std::chrono;

int* coordinate_convert(string coordinate);

char number_to_letter(int num);

void clear_screen();

class Board {
	int height;
	int width;
	char empty_field;
	char taken_field;
	char hit_field;
	char missed_field;
	int ship_number;
	int* ship_sizes;
public:
	void set(int width, int height, char empty_field, char taken_field, char hit_field, char missed_field, int ship_number);
	int get_height() { return height; }
	int get_width() { return width; }
	char get_empty_field() { return empty_field; }
	char get_taken_field() { return taken_field; }
	char get_hit_field() { return hit_field; }
	char get_missed_field() { return missed_field; }
	int get_ship_number() { return ship_number; }
	int* get_ship_sizes() { return ship_sizes; }
};