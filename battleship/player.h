#include "board.h"

class Player {
protected:
	string username;
	int** ships;

public:
	string get_username() { return username; }
	void set_username(string str) { username = str; }
	int** get_ships() { return ships; }

	void print_board(Board b);

	virtual bool add_ship(int size, Board b) = 0;

	bool overlap_check(Board b, int** new_ship);

	bool check_if_overboard(Board b, int x, int y, bool position, int size);

	void set_new_ship(Board b, int** new_ship);

	void set_ships(Board b);

	bool is_hit(int x, int y);

	bool hit_same_field_check(int x, int y);

	bool check_move(Board b, Player& enemy, int x, int y);
};

class HumanPlayer : public Player {
public:
	HumanPlayer();

	bool input_new_ship(string& coordinate, bool& position, int size);

	bool add_ship(int size, Board b);
};

class ComputerPlayer : public Player {
public:
	ComputerPlayer() { username = "Computer"; }

	bool add_ship(int size, Board b);

	int* smart_move(Board b, Player& enemy);
};
