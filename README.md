# Battleship


## Table of Contents
* [Description](https://gitlab.com/iz46273/battleship#description)
* [Technologies](https://gitlab.com/iz46273/battleship#technologies)


### Description:
* 2 players: user and computer
* players set their ships
* player can make a hit by entering coordinates
* if the ship is hit, it will be shown on board
* the winner is the first one who sinks all of the oponent's ships

### Technologies:
C++ console game
