#include "player.h"

class Game {
	HumanPlayer player;
	ComputerPlayer computer;
	Board b;
	int total_points;
	int player_points;
	int computer_points;

public:
	Game();
	
	void start();
};
