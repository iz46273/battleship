#include "board.h"

void Board::set(int width, int height, char empty_field, char taken_field, char hit_field, char missed_field, int ship_number) {
	this->width = width;
	this->height = height;
	this->empty_field = empty_field;
	this->taken_field = taken_field;
	this->hit_field = hit_field;
	this->missed_field = missed_field;
	this->ship_number = ship_number;
	ship_sizes = new int[ship_number];
	for (int i = 0; i < ship_number; i++) {
		int random = 2 + (rand() % (5 - 2 + 1));
		ship_sizes[i] = random;
	}
}

int* coordinate_convert(string coordinate) {
	coordinate[0] = toupper(coordinate[0]);
	coordinate[1] = toupper(coordinate[1]);
	int coordinate_x = coordinate[1] - 65;
	int coordinate_y = coordinate[0] - 65;

	int coordinates[2] = { coordinate_x, coordinate_y };
	return coordinates;
}

char number_to_letter(int num) {
	string str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char letter = str[num];
	return letter;
}

void clear_screen() {
	std::cout << "\x1B[2J\x1B[H";
}